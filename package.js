Package.describe({
	name: 'howcloud:accounts-guest',
	summary: "Creates guest account registration which creates an account for every visitor as well as claim system to allow the user to claim their guest account"
});

Package.on_use(function (api) {
	
	/* Package Dependencies */

	api.use('underscore');
	api.use('ddp');
	api.use('mongo');
	api.use('accounts-base');
	api.use('accounts-password'); // this defines Accounts.createUser for example
	api.use('check');

	api.use('howcloud:react'); // for the server side user system
	api.use('howcloud:react-email');
	api.use('howcloud:users'); // requires the underlying howcloud users framework (we extend it too)

	/* Add files */

	// main lib

	api.add_files('guest_client.js', 'client');
	api.add_files('guest_server.js', 'server');

	// users collection

	api.add_files('users_shared.js', ['client', 'server']);

});