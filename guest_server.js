/** Guest Accounts **/

// Creates a guest account container on the server which has a random username
// We then log the user into this account using a token

Accounts.createGuestAccount = function () {
	var uuid = Users.generateUsernameFromString(Random.id());

	var userId = Accounts.createUser({
		username: uuid,
		// password: uuid, // no password => cannot just be logged into from the username
		profile: {guest: true} // we set this in the profile too so that it is published and available in Meteor.user() by default
	});

	Meteor.users.update(userId, {$set: {guest: true}}); // can this just be set as part of above -^

	var stampedLoginToken = Accounts._generateStampedLoginToken();
	Accounts._insertLoginToken(userId, stampedLoginToken);

	return stampedLoginToken.token;
}

Meteor.methods({
	accounts_createGuestAccount: function () {
		if (Meteor.user()) return null;

		return Accounts.createGuestAccount();
	},
});

// Using the below might be dangerous - we develop dependencies to certain users even if they are not logged in, in this case don't delete!
// We could store a flag if this user has dependencies (ie posted a feed item, classroom etc)

Accounts.removeOldGuests = function (before) {
	if (typeof before === 'undefined') {
		before = new Date();
		before.setHours(before.getHours() - 12);
	}

	res = Meteor.users.remove({
		createdAt: {$lte: before},
		guest: true
	});

	return res;
}

Accounts.claimGuestAccount = function (user_id) {
	// we assume they actually were a guest - ie we need to make sure that this is only called when we are actually claiming an account

	Users.collection.update(user_id, {
		$unset: {
			'profile.guest': '',
			'guest': ''
		}
	});

	return true;	
}

/** External Service Override **/
// Only create new user accounts for an external service if the user is not logged in - if not we are adding another service to the account
// Requires an override (from http://stackoverflow.com/questions/15592965/how-to-add-external-service-logins-to-an-already-existing-account-in-meteor)

var orig_updateOrCreateUserFromExternalService = Accounts.updateOrCreateUserFromExternalService;
Accounts.updateOrCreateUserFromExternalService = function(serviceName, serviceData, options) {
	var noExisting = false; // was there no existing user when we started this function ?
	var user = Users.getServiceUser(serviceName, serviceData);

	// Facebook specific - extend the access token automatically:
	// Note: we call this on every login as this will still expire every 60 days or so, so always worth extending
	if (serviceName == 'facebook') Facebook.extendAccessToken(serviceData.accessToken);

	if (!user) { // ie we only add to the current user if another user is not already setup with this account
		noExisting = true;

		if (!options || !options._FORCE_NEW_ACCOUNT) {
			var loggedInUser = Meteor.user();

			if(loggedInUser) {
				if (typeof(loggedInUser.services[serviceName]) === "undefined") {
					var setAttr = {};
					var unsetAttr = {};
					
					// Service information
					setAttr["services." + serviceName] = serviceData;

					// If the user was previously seen as a guest - they should not be now
					if (loggedInUser.isGuest()) Accounts.claimGuestAccount(loggedInUser._id);

					Meteor.users.update(loggedInUser._id, {$set: setAttr});
				
					// We update the user's profile in virtue of the noExisting flag being tripped - see end of function
				}
			}
		}
	}

	if (options) delete options._FORCE_NEW_ACCOUNT;

	var rt = orig_updateOrCreateUserFromExternalService.apply(this, arguments);

	//if (noExisting) {
		// If there was no existing user for this service when we started the function then we must have created or attached the user to another etc
		// => refresh their profile now they have a new external service added

		// CHANGED: We now always update the user profile - we just ensure that those changes only take effect if there is nothing entered in the profile by default
		// Not sure if there are any negative consequences of this. Probably not. But it doesn't fix our issue with fb emails not being set on users for some reason.
		// TODO: Wrap this is Meteor.setTimeout ???

		var extUser = Users.getServiceUser(serviceName, serviceData);
		extUser.updateProfileFromExternalServices();		
	//}

	return rt;
}

Users.extendDocument({

	// default updateProfileFromExternalServices:
	// This should be overriden to implement functionality to grab profile pictures etc which is app dependent, can always call using this._super anyway
	// We just deal with basics like setting names/email

	updateProfileFromExternalServices: function () {

		if (this.services && this.services.facebook) { // fb external service

			// Name:

			if (!this.profile.name) this.setName(this.services.facebook.name); // set through method (deals with pushing codename etc)

			// Email:

			if (this.services.facebook.email) this.pushEmail(this.services.facebook.email, true); 

		}

	},

});

/** Sign Up Method **/

// Formally signs a user up a user - creating an account or claiming a guest account
// We use this rather than calling createUser on the client so that we merely update an existing guest user (if they exist)
// Avoids us having to swizzle an existing method like we do for external services [which makes sense in that case due to the abstracted nature of external services]

// Less flexible than createUser because we simply expect a name, email, password to be submitted
// We generate the default username from the name we are given
// If we ever want to implement email verification we will have to do it at this level instead of (or as well as [?]) at the underlying accounts-password system level

Accounts.signup = function (data) { // this is defined and works both on server and client
									// note that on the server this always creates rather than claims an account and we return a user_id
									// we define this on server too because it deals with some of the functionaliy that calling createUser does not - ie generating passwords, usernames etc if necessary
									// => minimum requirement for this to run is a name and email
									
									// TODO: do some refactoring here

	/** Generate Password **/

	var generatedPassword = false;
	if (!data.password) {
		generatedPassword = true;
		data.password = randString(6);
	}

	/** Generate Username **/

	var _username = data.username;
	if (!_username && data.name) _username = Users.generateUsernameFromString(data.name);

	/** Create Account **/

	var user_id = Accounts.createUser({
		email: data.email,
		username: _username,
		password: data.password,
		profile: {
			name: data.name
		}
	});

	if (generatedPassword) {
		// Don't really care about this - we just get them to reset it
		// Users.collection.update(user_id, {
		// 	$set: {
		// 		'services.password.placeholder': true
		// 	}
		// });

		if (data.email) {
			var user = Users.getById(user_id);

			EmailHooks.call("user_generatedSignUpPassword", {
				user_id: user_id,
				token: user.createPasswordResetToken()
			});
		}
	}

	/** Return **/

	return user_id;

}

Meteor.methods({

	signup: function (data) {

		data = data || {}
		var user = Meteor.user();

		/** Validate Data **/

		// clean data
		// null !== undefined and so does not pass the check... must be another way around this

		if (!data.password) delete data.password;
		if (!data.username) delete data.username; 

		// check

		check(data, Match.ObjectIncluding({
			username: Match.Optional(String),
			email: String,
			password: Match.Optional(String),
			name: String
		}));

		/** Generate Password **/

		// if the user has not specified a password then we generate one and we mark the account as needing the user to set a password
		// the way this will be done is via the password reset system (ie asking the user to go to a certain page to set a password)

		var generatedPassword = false;
		if (!data.password) {
			generatedPassword = true;
			data.password = randString(6);
		}

		/** Generate Username **/

		var _username = data.username;
		if (!_username && data.name) _username = Users.generateUsernameFromString(data.name);

		/** Process **/

		var user_id = user ? user._id : null; // want to populate this if we are creating a new account
		var _return; // what to return

		if (!user || !user.isGuest()) { // if there is no existing user or the existing user is not a guest - create a new account

			user_id = Accounts.createUser({
				email: data.email,
				username: _username,
				password: data.password,
				profile: {
					name: data.name
				}
			});

			if (!user_id) throw new Meteor.Error(403, "createUser failed to insert new user"); // copying from accounts-password package

			// Return a token we can use to log the user in with (copy pasted from guest account generation)

			var stampedLoginToken = Accounts._generateStampedLoginToken();
			Accounts._insertLoginToken(user_id, stampedLoginToken);

			_return = stampedLoginToken.token;

		} else { // user is logged into a guest account => claim

			if (!user.pushEmail(data.email)) throw new Meteor.Error(403, "Email already exists."); // we match the error text to be the same as if createUser had been called

			Accounts.setPassword(user._id, data.password, {
				logout: false // do not logout user when the password is changed
			});

			var sets = {
				'profile.name': data.name,
				'username': _username
			}

			Users.collection.update(user._id, {
				$set: sets
			});

			// No need to actively log the user into this claimed account - they are already logged into it!

			Accounts.claimGuestAccount(user._id); // ie strip out stuff which identifies this as a guest account

			_return = 'claimed'; // => no need to log the user into a different account

		}

		/** Generated Password Updates **/

		if (generatedPassword) {
			// We don't really care about this - we just treat it like a default password which needs changing
			// Users.collection.update(user_id, {
			// 	$set: {
			// 		'services.password.placeholder': true
			// 	}
			// });

			if (data.email) {
				var user = Users.getById(user_id);

				EmailHooks.call("Reset Generated Signup Password", {
					user_id: user_id,
					token: user.createPasswordResetToken()
				});
			}
		}

		/** Return **/

		return _return;

	}

});

EmailHooks.register('Reset Generated Signup Password', function (data) {
	var user = Users.getById(data.user_id);

	var email = user.newEmail('HowCloud: Create Your Password');
	if (!email) return false;

	email.setContent([
		'Hi '+user.firstName()+',<br /><br />',
		'Welcome to HowCloud! To be able to login to your account at any time you need to set a password.<br /><br />',
		'<a class="button button-large" href="'+HC.web_url()+'reset/'+user._id+'/'+data.token+'">Click here to setup your account password</a><br /><br />',
		'We can\'t wait for you to start learning with us on HowCloud!<br /><br/>',
		'~ The HowCloud team'
	].join(''));

	return email.send();
});