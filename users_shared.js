Users.extendDocument({

	/** Guest account specific methods **/

	isGuest: function () {
		return (this.guest || (this.profile && this.profile.guest) ? true : false);
	},

});