/** Guests Accounts **/

Meteor.startup(function () {
	Deps.autorun(function () { // create a guest account for the user if they are not logged in (or indeed in the event they logout)
		if (!Meteor.userId()) {
			Deps.nonreactive(function () {
				Accounts.createGuest();
			});
		}
	});
});

Accounts.createGuest = function (fn) {
    if (Meteor.user()) return;

	Meteor.call("accounts_createGuestAccount", function (err, result) {
        // result is a login token (if created)

		if (result) Meteor.loginWithToken(result);
	});
}

/** Sign Up **/

Accounts.signup = function (data, __callback) {
    Meteor.call("signup", data, function (err, result) {
        if (err) {
            if (__callback) __callback(err, result);
        } else {
            if (result === 'claimed') { 
                if (__callback) __callback(err, result);
            } else { // => requires us to login to the newly created account
                Meteor.loginWithToken(result, __callback);
            }
        }
    });
}
